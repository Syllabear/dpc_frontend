
DEST_DIR = "D:/Docker Toolbox/DPC/front-end/app/public"

import os
import os.path
import shutil
import fnmatch

dest_dir = 'D:\\Docker Toolbox\\DPC\\front-end\\app\\'     # folder for the destination of the copy

def copytree(src, dst, symlinks=False, ignore=None):
    if not os.path.exists(dst):
        os.makedirs(dst)
    for item in os.listdir(src):
        s = os.path.join(src, item)
        d = os.path.join(dst, item)
        if os.path.isdir(s):
            copytree(s, d, symlinks, ignore)
        else:
            if not os.path.exists(d) or os.stat(s).st_mtime - os.stat(d).st_mtime > 1:
                shutil.copy2(s, d)

copytree("src", dest_dir + "src")
copytree("public", dest_dir + "public")