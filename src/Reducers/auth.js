import {
  DEINSCRIPTION_SUCCESS,
  INSCRIPTION_SUCCESS,
  LOAD_FAILED,
  LOAD_SUCCESS,
  LOGIN_FAILED,
  LOGIN_SUCCESS,
  LOGOUT_SUCCESS,
  REGISTER_FAILED,
  REGISTER_SUCCESS,
  UPDATE_FAILED,
  UPDATE_SUCCESS
} from "../Actions/types";

const initialState = {
  username: "",
  token: localStorage.getItem("token"),
  group: "",
  filter: "",
  inscriptions: [],
  isAuth: false,
  id: null,
  errorsMsg: null
};

export default function(state = initialState, action) {
  switch (action.type) {
    case LOAD_SUCCESS:
      return {
        ...state,
        username: action.payload.user.username,
        group: action.payload.profile.group,
        filter: action.payload.profile.filter,
        id: action.payload.user.id,
        inscriptions: action.payload.profile.inscriptions,
        errorsMsg: null,
        isAuth: true
      };
    case LOGIN_SUCCESS:
    case REGISTER_SUCCESS:
      localStorage.setItem("token", action.payload.token);
      return {
        ...state,
        username: action.payload.user.username,
        group: action.payload.profile.group,
        filter: action.payload.profile.filter,
        id: action.payload.user.id,
        inscriptions: action.payload.profile.inscriptions,
        errorsMsg: null,
        isAuth: true
      };
    case LOGIN_FAILED:
    case REGISTER_FAILED:
    case LOGOUT_SUCCESS:
      localStorage.removeItem("token");
      return {
        ...state,
        username: null,
        group: null,
        filter: null,
        token: "",
        isAuth: false,
        inscriptions: [],
        id: null,
        errorsMsg: action.payload
      };
    case LOAD_FAILED:
      localStorage.removeItem("token");
      return {
        ...state,
        username: null,
        group: null,
        filter: null,
        token: "",
        isAuth: false,
        inscriptions: [],
        id: null,
        errorsMsg: null
      };
    case INSCRIPTION_SUCCESS:
      return {
        ...state,
        inscriptions: [...state.inscriptions, action.payload]
      };
    case DEINSCRIPTION_SUCCESS:
      return {
        ...state,
        inscriptions: state.inscriptions.filter(val => val != action.payload)
      };
    case UPDATE_SUCCESS:
    case UPDATE_FAILED:
    default:
      return state;
  }
}
