import { combineReducers } from "redux";
import AuthReducer from "./auth";
import SessionReducer from "./session";

export default combineReducers({
  AuthReducer,
  SessionReducer
});
