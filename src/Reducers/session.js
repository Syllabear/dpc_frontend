import {
  ADD_SESSION_SUCCESS,
  DEL_SESSION_SUCCESS,
  LOAD_SESSION_FAILED,
  LOAD_SESSION_SUCCESS
} from "../Actions/types";

const initialState = {
  sessions: []
};

export default function(state = initialState, action) {
  switch (action.type) {
    case ADD_SESSION_SUCCESS:
      return {
        ...state,
        sessions: [...state.sessions, action.payload]
      };
    case LOAD_SESSION_SUCCESS:
      return {
        ...state,
        sessions: action.payload
      };
    case DEL_SESSION_SUCCESS:
      return {
        ...state,
        sessions: state.sessions.filter(
          session => session.pk !== action.payload
        )
      };
    case LOAD_SESSION_FAILED:
      return state;
    default:
      return state;
  }
}
