import React, { Component, Fragment } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { logout } from "../Actions/auth";

class Header extends Component {
  // static PropTypes = {
  //   username: PropTypes.
  // }

  render() {
    let headerButton = "";
    if (!this.props.auth.isAuth) {
      headerButton = (
        <div className="collapse navbar-collapse" id="navbarColor01">
          <ul className="navbar-nav ml-auto">
            <li className="nav-item">
              <Link to="/about" className="nav-link">
                A Propos <span className="sr-only">(current)</span>
              </Link>
            </li>
            <li className="nav-item">
              <Link to="/photos" className="nav-link">
                Photos <span className="sr-only">(current)</span>
              </Link>
            </li>
            <li className="nav-item">
              <Link to="/login" className="nav-link">
                Connexion
              </Link>
            </li>
            <li className="nav-item">
              <Link to="/register" className="nav-link">
                Inscription
              </Link>
            </li>
          </ul>
        </div>
      );
    } else {
      headerButton = (
        <div className="collapse navbar-collapse" id="navbarColor01">
          <ul className="navbar-nav ml-auto">
            <li className="nav-item">
              <Link to="/" className="nav-link">
                Accueil <span className="sr-only">(current)</span>
              </Link>
            </li>
            <li className="nav-item">
              <Link to="/about" className="nav-link">
                A Propos <span className="sr-only">(current)</span>
              </Link>
            </li>
            <li className="nav-item">
              <Link to="/photos" className="nav-link">
                Photos <span className="sr-only">(current)</span>
              </Link>
            </li>
            <li className="nav-item">
              <Link
                to="/login"
                onClick={this.props.logout}
                className="nav-link"
              >
                Déconnexion
              </Link>
            </li>
          </ul>
        </div>
      );
    }
    return (
      <Fragment>
        <nav className="navbar navbar-default fixed-top navbar-expand-sm navbar-dark bg-dark">
          <div className="container-fluid">
            <div className="navbar-brand ml-4">Dagneux Paintball Club</div>
            <button
              className="navbar-toggler"
              type="button"
              data-toggle="collapse"
              data-target="#navbarColor01"
              aria-controls="navbarColor01"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span className="navbar-toggler-icon" />
            </button>
            {headerButton}
          </div>
        </nav>
        <div style={{ paddingTop: "10px", paddingBottom: "56px" }} />
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.AuthReducer
});

export default connect(
  mapStateToProps,
  { logout }
)(Header);
