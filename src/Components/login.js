import React, { Component } from "react";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
// import PropTypes from "prop-types";
import { login } from "../Actions/auth";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: ""
    };
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  onSubmit = e => {
    e.preventDefault();
    this.props.login(this.state.username, this.state.password);
  };

  render() {
    if (this.props.isAuth) {
      return <Redirect to="/home" />;
    }

    let userClass = "form-control ";
    let passwordClass = "form-control ";
    let loginError = false;

    if (this.props.errorsMsg) {
      for (let key in this.props.errorsMsg.msg) {
        switch (key) {
          case "username":
            userClass = userClass + "is-invalid";
            break;
          case "password":
            passwordClass += "is-invalid";
            break;
          case "non_field_errors":
            loginError = true;
            break;
          default:
        }
      }
    }

    const input = (
      <input
        type="text"
        value={this.state.username}
        onChange={this.onChange}
        name="username"
        className={userClass}
        id="username"
        placeholder="Nom d'utilisateur ..."
      />
    );

    const password = (
      <input
        type="password"
        name="password"
        value={this.state.password}
        onChange={this.onChange}
        className={passwordClass}
        id="password"
        placeholder="........."
      />
    );

    const errorAlert = (
      <div className="alert alert-dismissible alert-danger">
        <button type="button" className="close" data-dismiss="alert">
          &times;
        </button>
        <strong>Nom d'utilisateur ou mot de passe incorrect !</strong>
      </div>
    );

    return (
      <div className="card card-body m-auto col-6">
        <h1>Connexion</h1>
        <form>
          {loginError ? errorAlert : ""}
          <div className="form-group">
            <label htmlFor="exampleFormControlInput1">
              Nom d'utilisateur :
            </label>
            {input}
            <div className="invalid-feedback">Nom d'utilisateur vide !</div>
          </div>
          <div className="form-group">
            <label htmlFor="exampleFormControlInput1">Mot de passe :</label>
            {password}
            <div className="invalid-feedback"> Mot de passe vide ! </div>
          </div>
          <button
            type="submit"
            onClick={this.onSubmit}
            className="btn btn-primary col-xl mb-4"
          >
            Connexion
          </button>
          <p className="mb-0">
            Pas encore inscrit ? <Link to="/register"> Inscription </Link>
          </p>
        </form>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  isAuth: state.AuthReducer.isAuth,
  username: state.AuthReducer.username,
  errorsMsg: state.AuthReducer.errorsMsg
});

export default connect(
  mapStateToProps,
  { login }
)(Login);
