import React, { Component } from "react";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
// import PropTypes from "prop-types";
import { register, updateProfile } from "../Actions/auth";

class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      email: "",
      password: "",
      password2: "",
      group: "0",
      error: false
    };

    this.onClick = this.onClick.bind(this);
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.checkData = this.checkData.bind(this);
    this.checkPassword = this.checkPassword.bind(this);
    this.checkPassword2 = this.checkPassword2.bind(this);
    this.checkUserName = this.checkUserName.bind(this);
  }

  checkPassword = () => {
    return this.state.password.length > 0;
  };

  checkPassword2 = () => {
    return this.state.password === this.state.password2;
  };

  checkUserName = () => {
    return this.state.username !== "";
  };

  checkData = () => {
    return (
      this.checkPassword() && this.checkPassword2() && this.checkUserName()
    );
  };

  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  onClick = e => {
    e.preventDefault();
    this.setState({
      group: e.target.id
    });
  };

  onSubmit = e => {
    e.preventDefault();

    if (this.checkData()) {
      this.setState({ error: false });
      this.props.register(
        this.state.username,
        this.state.email,
        this.state.password,
        this.state.group
      );
    } else {
      this.setState({ error: true });
    }
  };

  render() {
    if (this.props.isAuth) {
      return <Redirect to="/home" />;
    }

    let group = "";

    switch (this.state.group) {
      case "0":
        group = "8-12 ans";
        break;
      case "1":
        group = "12-16 ans";
        break;
      case "2":
        group = "Adulte";
        break;
      default:
        group = "8-12 ans";
    }

    let username = "form-control";
    let password = "form-control";
    let password2 = "form-control";

    if (this.state.error) {
      console.log("error");
      if (!this.checkUserName()) username += " is-invalid";
      if (!this.checkPassword()) password += " is-invalid";
    }
    if (!this.checkPassword2()) password2 += " is-invalid";

    return (
      <div className="card card-body m-auto col-6">
        <h1> Formulaire d'inscription </h1>
        <form>
          <div className="form-group">
            <label htmlFor="exampleFormControlInput1">
              Nom d'utilisateur :
            </label>
            <input
              type="text"
              value={this.state.username}
              onChange={this.onChange}
              name="username"
              className={username}
              placeholder="Nom d'utilisateur..."
            />
            <div className="invalid-feedback">
              Le nom d'utilisateur ne peut être vide !
            </div>
          </div>
          <div className="form-group">
            <label htmlFor="exampleFormControlInput1">Email : </label>
            <input
              type="email"
              value={this.state.email}
              onChange={this.onChange}
              name="email"
              className="form-control"
              placeholder="test@gmail.com"
            />
          </div>
          <div className="form-group">
            <label htmlFor="exampleFormControlInput1">Mot de passe :</label>
            <input
              type="password"
              name="password"
              value={this.state.password}
              onChange={this.onChange}
              className={password}
              placeholder="........."
            />
            <div className="invalid-feedback">
              Le mot de passe ne peut être vide !
            </div>
          </div>
          <div className="form-group">
            <label htmlFor="exampleFormControlInput1">
              Confirmer le mot de passe :
            </label>
            <input
              type="password"
              name="password2"
              value={this.state.password2}
              onChange={this.onChange}
              className={password2}
              placeholder="........."
            />
            <div className="invalid-feedback"> Mot de passes différents </div>
          </div>
          <div className="dropdown mb-2">
            <label> Groupe :</label>
            <button
              className="btn col-xl btn-secondary dropdown-toggle "
              type="button"
              id="dropdownMenu2"
              data-toggle="dropdown"
              aria-haspopup="true"
              aria-expanded="false"
              name="group"
            >
              {group}
            </button>
            <div className="dropdown-menu" aria-labelledby="dropdownMenu2">
              <button
                className="dropdown-item"
                type="button"
                onClick={this.onClick}
                id={0}
              >
                8-12 ans
              </button>
              <button
                className="dropdown-item"
                type="button"
                onClick={this.onClick}
                id={1}
              >
                12-16 ans
              </button>
              <button
                className="dropdown-item"
                type="button"
                onClick={this.onClick}
                id={2}
              >
                Adulte
              </button>
            </div>
          </div>
          <div className="mb-2">
            <button
              type="submit"
              onClick={this.onSubmit}
              className="btn btn-primary  mb-4 col-xl"
            >
              Inscription
            </button>
          </div>
          <p>
            Déjà un compte ? <Link to="/login"> Connexion </Link>
          </p>
        </form>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  isAuth: state.AuthReducer.isAuth,
  username: state.AuthReducer.username,
  errors: state.AuthReducer.errorsMsg
});

export default connect(
  mapStateToProps,
  { register, updateProfile }
)(Register);
