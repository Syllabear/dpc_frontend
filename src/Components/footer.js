import React, { Component, Fragment } from "react";

class Footer extends Component {
  render() {
    return (
      <Fragment>
        <div style={{ paddingTop: "100px", paddingBottom: "0px" }} />
        <nav className="navbar navbar-dark navbar-default fixed-bottom bg-dark ">
          <hr />
          <div className="container-fluid">
            <a
              className="m-auto"
              href="https://www.facebook.com/Dagneux-Paintball-Club-2089698221355218/"
            >
              <i
                // style={{ background: "white" }}
                className="fa fa-facebook-square fa-3x social"
              />
            </a>
            {/*<a className="col-md-2" href="mailto:bootsnipp@gmail.com">*/}
            {/*  <i className="fa fa-envelope-square fa-3x social" />*/}
            {/*</a>*/}
          </div>
          {/*<hr />*/}
          <br />
        </nav>
      </Fragment>
    );
  }
}

export default Footer;
