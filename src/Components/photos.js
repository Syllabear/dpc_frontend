import React, { Component } from "react";
import img1 from "../Assets/IMG_001.jpg";
import img2 from "../Assets/IMG_002.jpg";
import img4 from "../Assets/IMG_004.jpg";
import img5 from "../Assets/IMG_005.jpg";
import img6 from "../Assets/IMG_006.jpg";
import img8 from "../Assets/IMG_008.jpg";
import img9 from "../Assets/IMG_009.jpg";
import img10 from "../Assets/IMG_010.jpg";

class Photos extends Component {
  render() {
    return (
      <div className="container-fluid m-auto">
        <img className="col-lg-3 col-md-6 col-sm-12 mt-3" src={img1} alt={""} />
        <img className="col-lg-3 col-md-6 col-sm-12 mt-3" src={img2} alt={""} />
        <img className="col-lg-3 col-md-6 col-sm-12 mt-3" src={img9} alt={""} />
        <img className="col-lg-3 col-md-6 col-sm-12 mt-3" src={img4} alt={""} />
        <img className="col-lg-3 col-md-6 col-sm-12 mt-3" src={img5} alt={""} />
        <img className="col-lg-3 col-md-6 col-sm-12 mt-3" src={img6} alt={""} />
        <img className="col-lg-3 col-md-6 col-sm-12 mt-3" src={img8} alt={""} />
        <img
          className="col-lg-3 col-md-6 col-sm-12 mt-3"
          src={img10}
          alt={""}
        />
      </div>
    );
  }
}

export default Photos;
