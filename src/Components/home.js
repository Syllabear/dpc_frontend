import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import { loadSession, deleteSession, addSession } from "../Actions/session";
import { updateInscription } from "../Actions/auth";
import PropTypes from "prop-types";
import Modal from "react-modal";

const uuidv1 = require("uuid/v1");

class Home extends Component {
  static propTypes = {
    myId: PropTypes.number.isRequired,
    sessions: PropTypes.array.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {
      modalIsOpen: false
    };
    this.onClick = this.onClick.bind(this);
    this.openModal = this.openModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.participer = this.participer.bind(this);
    this.nePasParticiper = this.nePasParticiper.bind(this);
  }

  componentDidMount() {
    this.props.loadSession();
  }

  onClick(e, pk) {
    this.props.deleteSession(pk);
  }

  openModal(e) {
    e.preventDefault();
    this.setState({ modalIsOpen: true });
  }

  closeModal(e) {
    this.setState({ modalIsOpen: false });
  }

  participer(e, pk) {
    this.props.updateInscription(true, this.props.inscriptions, pk);
  }

  nePasParticiper(e, pk) {
    this.props.updateInscription(false, this.props.inscriptions, pk);
  }

  render() {
    //TODO FILTRE, MEILLEUR AFFICHAGE

    return (
      <div className="col-xl">
        <h2>Sessions</h2>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Titre</th>
              <th>Organisateur</th>
              <th>Description</th>
              <th>date</th>
              <th />
            </tr>
          </thead>
          <tbody>
            {this.props.sessions.map(session => (
              <tr key={uuidv1()}>
                <td> {session.titre} </td>
                <td> {session.owner.username} </td>
                <td> {session.description} </td>
                <td> {new Date(session.date).toLocaleDateString()} </td>
                <td>
                  {!this.props.inscriptions.includes(session.pk) ? (
                    <button
                      className="btn btn-success btn-sm"
                      onClick={() => {
                        this.participer(this, session.pk);
                      }}
                    >
                      Participer
                    </button>
                  ) : (
                    <button
                      className="btn btn-danger btn-sm"
                      onClick={() => {
                        this.nePasParticiper(this, session.pk);
                      }}
                    >
                      Ne pas participer
                    </button>
                  )}
                </td>
                {this.props.myId === session.owner.id ? (
                  <td>
                    <button
                      type="button"
                      className="btn btn-danger btn-sm"
                      onClick={() => {
                        this.onClick(this, session.pk);
                      }}
                    >
                      Supprimer
                    </button>
                  </td>
                ) : (
                  <td>{}</td>
                )}
              </tr>
            ))}
          </tbody>
        </table>
        {this.props.group === 2 ? (
          <Fragment>
            <button
              type="button"
              className="btn btn-success btn-sm"
              onClick={this.openModal}
            >
              Ajouter une session
            </button>
            <div id="modal" />
            <CreateSession
              owner={this.props.myId}
              onSubmit={this.props.addSession}
              openModal={this.state.modalIsOpen}
              onClose={this.closeModal}
            />
          </Fragment>
        ) : (
          ""
        )}
      </div>
    );
  }
}

//TODO Améliorer modal
class CreateSession extends Component {
  constructor(props) {
    super(props);

    this.state = {
      titre: "Training",
      description: "Training",
      lieu: "Dagneux",
      date: new Date().toISOString().split("T")[0]
    };
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  onSubmit = e => {
    e.preventDefault();

    this.props.onClose();

    this.props.onSubmit(
      this.props.owner,
      this.state.date,
      this.state.description,
      this.state.titre,
      this.state.lieu
    );
  };

  componentDidMount() {
    Modal.setAppElement("#modal");
  }

  render() {
    const customStyles = {
      overlay: {
        top: "50px"
      },
      content: {
        top: "50px"
      }
    };

    return (
      <Modal
        className="container m-auto "
        style={customStyles}
        isOpen={this.props.openModal}
        onRequestClose={this.props.onClose}
        contentLabel="Example Modal"
      >
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLabel">
                Nouvelle session
              </h5>
              <button
                type="button"
                onClick={this.props.onClose}
                className="close"
                aria-label="Close"
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              <div className="card card-body m-auto">
                <form>
                  {/*TITRE*/}
                  <div className="form-group">
                    <label htmlFor="exampleFormControlInput1">Titre :</label>
                    <input
                      type="text"
                      value={this.state.titre}
                      onChange={this.onChange}
                      name="titre"
                      className="form-control"
                      placeholder="Training U12"
                    />
                    <div className="invalid-feedback">
                      Nom d'utilisateur vide !
                    </div>
                  </div>
                  {/*LIEU*/}
                  <div className="form-group">
                    <label htmlFor="exampleFormControlInput1">Lieu :</label>
                    <input
                      value={this.state.lieu}
                      type="text"
                      name="lieu"
                      onChange={this.onChange}
                      className="form-control"
                      placeholder="Dagneux"
                    />
                  </div>
                  {/*DATE*/}
                  <div className="form-group">
                    <label htmlFor="exampleFormControlInput1">Date :</label>
                    <input
                      type="date"
                      value={this.state.date}
                      name="date"
                      onChange={this.onChange}
                      className="form-control"
                    />
                  </div>
                  {/*DESCRIPTION*/}
                  <div className="form-group">
                    <label htmlFor="exampleFormControlInput1">
                      Description :
                    </label>
                    <textarea
                      value={this.state.description}
                      name="description"
                      onChange={this.onChange}
                      className="form-control"
                    />
                  </div>
                </form>
              </div>
            </div>
            <div className="modal-footer">
              <button
                data-toggle="modal"
                data-target="#exampleModal"
                type="button"
                className="btn btn-primary"
                onClick={this.onSubmit}
              >
                Ajouter
              </button>
            </div>
          </div>
        </div>
      </Modal>
    );
  }
}

const mapStateToProps = state => ({
  myId: state.AuthReducer.id,
  group: state.AuthReducer.group,
  inscriptions: state.AuthReducer.inscriptions,
  sessions: state.SessionReducer.sessions
});

export default connect(
  mapStateToProps,
  { loadSession, deleteSession, addSession, updateInscription }
)(Home);
