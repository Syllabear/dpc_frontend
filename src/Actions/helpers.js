export function loadConfig() {
  let token = null;
  token = localStorage.getItem("token");

  const config = {
    headers: {
      "Content-Type": "application/json"
    }
  };
  if (token) {
    config.headers["Authorization"] = `Token ${token}`;
  }
  return config;
}
