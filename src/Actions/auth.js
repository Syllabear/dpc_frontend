import axios from "axios";
import { loadConfig } from "./helpers";
import {
  LOAD_FAILED,
  LOAD_SUCCESS,
  LOGIN_FAILED,
  LOGIN_SUCCESS,
  REGISTER_FAILED,
  REGISTER_SUCCESS,
  LOGOUT_SUCCESS,
  UPDATE_SUCCESS,
  UPDATE_FAILED,
  INSCRIPTION_SUCCESS,
  INSCRIPTION_FAILED,
  DEINSCRIPTION_SUCCESS
} from "./types";
import { BASE_URL } from "./var";

//LOGIN
export const login = (username, password) => dispatch => {
  const config = loadConfig();
  const body = JSON.stringify({ username, password });

  axios
    .post(BASE_URL + "/api/auth/login", body, config)
    .then(res => {
      dispatch({
        type: LOGIN_SUCCESS,
        payload: res.data
      });
    })
    .catch(err => {
      const errors = {
        msg: err.response.data,
        status: err.response.status
      };
      dispatch({
        type: LOGIN_FAILED,
        payload: errors
      });
    })
    .catch(err => {
      console.error(err);
    });
};

export const updateProfile = group => (dispatch, getState) => {
  const config = loadConfig(getState);

  const body = JSON.stringify({ group, filter: group });

  axios
    .patch(BASE_URL + "/api/user/profile/", body, config)
    .then(res => {
      dispatch({
        type: UPDATE_SUCCESS
      });
    })
    .catch(err => {
      console.log(err);
      const errors = {
        msg: err.response.data,
        status: err.response.status
      };
      dispatch({
        type: UPDATE_FAILED,
        payload: errors
      });
    })
    .catch(err => {
      console.log(err);
      dispatch({
        type: UPDATE_FAILED
      });
    });
};

export const register = (username, email, password, group) => dispatch => {
  const config = loadConfig();

  const body = JSON.stringify({
    username,
    email,
    password,
    group,
    filter: group
  });

  axios
    .post(BASE_URL + "/api/auth/register", body, config)
    .then(res => {
      dispatch({
        type: REGISTER_SUCCESS,
        payload: res.data
      });
    })
    .catch(err => {
      const errors = {
        msg: err.response.data,
        status: err.response.status
      };
      dispatch({
        type: REGISTER_FAILED,
        payload: errors
      });
    })
    .catch(err => {
      console.error(err);
    });
};

export const loadUser = () => (dispatch, getState) => {
  const config = loadConfig(getState);

  axios
    .get(BASE_URL + "/api/auth/user", config)
    .then(res => {
      dispatch({
        type: LOAD_SUCCESS,
        payload: res.data
      });
    })
    .catch(err => {
      dispatch({
        type: LOAD_FAILED
      });
    });
};

export const logout = () => (dispatch, getState) => {
  const config = loadConfig(getState);

  axios
    .post(BASE_URL + "/api/auth/user/logout/", null, config)
    .then(res => {
      dispatch({
        type: LOGOUT_SUCCESS
      });
    })
    .catch(err => {
      dispatch({
        type: LOGOUT_SUCCESS
      });
    });
};

export const updateInscription = (participe, inscriptions, pk) => (
  dispatch,
  getState
) => {
  const config = loadConfig(getState);

  const body = participe
    ? JSON.stringify({ inscriptions: [...inscriptions, pk] })
    : JSON.stringify({
        inscriptions: inscriptions.filter(val => val != pk)
      });

  axios
    .patch(BASE_URL + "/api/user/profile/", body, config)
    .then(res => {
      dispatch({
        type: participe ? INSCRIPTION_SUCCESS : DEINSCRIPTION_SUCCESS,
        payload: pk
      });
    })
    .catch(err => {
      const errors = {
        msg: err.response.data,
        status: err.response.status
      };
      console.log(errors.msg);
      dispatch({
        type: INSCRIPTION_FAILED,
        payload: errors
      });
    })
    .catch(err => {
      dispatch({
        type: INSCRIPTION_FAILED
      });
    });
};
