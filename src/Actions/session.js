import axios from "axios";
import {
  LOAD_SESSION_SUCCESS,
  LOAD_SESSION_FAILED,
  DEL_SESSION_SUCCESS,
  DEL_SESSION_FAILED,
  ADD_SESSION_FAILED,
  ADD_SESSION_SUCCESS
} from "./types";
import { loadConfig } from "./helpers";
import { BASE_URL } from "./var";

export const loadSession = () => (dispatch, getState) => {
  const config = loadConfig(getState);

  axios
    .get(BASE_URL + "/api/session/", config)
    .then(res => {
      dispatch({
        type: LOAD_SESSION_SUCCESS,
        payload: res.data
      });
    })
    .catch(err => {
      dispatch({
        type: LOAD_SESSION_FAILED,
        payload: err.data
      });
    });
};

export const deleteSession = pk => (dispatch, getState) => {
  const config = loadConfig(getState);

  axios
    .delete(BASE_URL + "/api/session/" + pk + "/del", config)
    .then(res => {
      dispatch({
        type: DEL_SESSION_SUCCESS,
        payload: pk
      });
      console.log(getState.sessions);
    })
    .catch(err => {
      dispatch({
        type: DEL_SESSION_FAILED,
        payload: err.data
      });
    });
};

export const addSession = (owner, date, description, titre, lieu) => (
  dispatch,
  getState
) => {
  const config = loadConfig(getState);

  const body = JSON.stringify({
    date,
    description,
    titre,
    lieu,
    owner
  });

  axios
    .post(BASE_URL + "/api/session/add", body, config)
    .then(res => {
      dispatch({
        type: ADD_SESSION_SUCCESS,
        payload: res.data.session
      });
    })
    .catch(err => {
      dispatch({
        type: ADD_SESSION_FAILED,
        payload: err.data
      });
    });
};
