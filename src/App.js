import React from "react";
import "./App.css";
import Header from "./Components/header";
import { Provider } from "react-redux";
import store from "./store";
import Login from "./Components/login";
import Home from "./Components/home";
import Register from "./Components/register";
import { loadUser } from "./Actions/auth";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import PrivateRoute from "./Common/privateRoute";
import About from "./Components/about";
import Footer from "./Components/footer";
import Photos from "./Components/photos";

class App extends React.Component {
  componentDidMount() {
    store.dispatch(loadUser());
  }

  render() {
    return (
      <Provider store={store}>
        <Router>
          <div className="App">
            <Header />

            <div className="container">
              <Switch>
                <PrivateRoute strict exact path="/" component={Home} />
                <PrivateRoute strict exact path="/home" component={Home} />
                <Route exact strict path="/login" component={Login} />
                <Route exact strict path="/register" component={Register} />
                <Route exact strict path="/photos" component={Photos} />
                <Route exact strict path="/about" component={About} />
              </Switch>
            </div>

            <Footer />
          </div>
        </Router>
      </Provider>
    );
  }
}

export default App;
